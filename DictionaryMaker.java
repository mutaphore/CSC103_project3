//DictionaryMaker.java
//Andy Chen, ID: 009433043 
//Michael Woodson, ID: 006470434
//Project 3
//CSC103 - Gharibyan - Section 11

import java.io.*;
import java.util.*;

//DictionaryMaker Class
//Read a text file indicated by the user, and generate a lexicographically sorted listing (increasing order) 
//of all distinct “words” contained in that file (duplicates should be ignored). The output is to go to a 
//text file also indicated by the user.
public class DictionaryMaker {

	public static void main(String[] args) {
		
		String inputFileName;
		String outputFileName;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter input file name: ");
		inputFileName = input.next();
		input.nextLine();
		System.out.print("Enter output file name: ");
		outputFileName = input.next();
		input.nextLine();
		
		BST<String> bst = new BST<String>();
		String temp;
		Scanner lineScanner;

		try {
			FileReader reader = new FileReader(inputFileName);
			Scanner in = new Scanner(reader);
			PrintWriter out = new PrintWriter(outputFileName);

			//Input from file and store into BST
			while(in.hasNextLine()) {
				lineScanner = new Scanner(in.nextLine());
				while(lineScanner.hasNext()) {
					temp = lineScanner.next();
					if(!bst.find(temp))
						bst.insert(temp);
					else if(lineScanner.hasNext())
						lineScanner.next();
				}
				lineScanner.close();
			}
			in.close();
			
			//Output to file, traversing BST with in-order Iterator
			Iterator<String> inItr = bst.iteratorIn();
		
			while(inItr.hasNext())
				out.println(inItr.next());
			
			out.close();
		}
		catch(IOException e) {
			System.out.print("Cannot read/write file");
		}
	
	}

}
