//LQueue.java
//Andy Chen, ID: 009433043 
//Michael Woodson, ID: 006470434
//Project 3
//CSC103 - Gharibyan - Section 11

//LQueue Class
//Represents a queue ADT. Includes enqueue, dequeue
//and isEmpty methods.
public class LQueue <T>
{
   private Node front;
   private Node end;
   
   //Node Class 
   private class Node
   {
      public T value;
      public Node next;
   }
   
   //MyException, an exception class that extends RuntimeException
   public static class MyException extends RuntimeException
   {
      public MyException()
      {
         super();
      }
      public MyException(String message)
      {
         super(message);
      }
   }
   
   //Constructor for LQueue
   public LQueue()
   {
      front = null;
      end = null;
   }
   
   //Enqueue, adds a new element into queue containing data
   public void enqueue (T data)
   {
      Node newNode = new Node();
      newNode.value = data;
      
      if(front != null)
      {         
         end.next = newNode;
         end = newNode;
      }
      else
      {
         front = newNode;
         end = newNode;
      }
   }
   
   //Dequeue, removes an element from queue and returns the data contained in it
   public T dequeue () 
   {
      T returnVal;
      
      if(front != null)
      {
         returnVal = front.value;
         front = front.next;
      }
      else
      {
         throw new MyException();
      }
      return returnVal;
   }
   
   //isEmpty, returns true if queue is empty, false if not
   public boolean isEmpty()
   {
      return front == null;
   }
   
}
