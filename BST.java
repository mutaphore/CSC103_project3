//BST.java
//Andy Chen, ID: 009433043 
//Michael Woodson, ID: 006470434
//Project 3
//CSC103 - Gharibyan - Section 11

import java.util.*;

//BST Class
//This class is the ADT for an binary search tree of generic types. It contains methods
//to manipulate/search/print data in the BST. It also can construct iterators to allow 
//the client to traverse through the elements in the tree in different orders.
public class BST<T extends Comparable <? super T>>
{
	
	//BSTNode Class
	//Node that contains data and pointers to its left and right children.
   private class BSTNode {

      T element;
      BSTNode left;
      BSTNode right;	
      
   }

   BSTNode root;
	
	//PreIter Class
	//Iterator for Preorder traversal of the BST
   private class PreIter implements Iterator<T>
   {  
      public MyStack<BSTNode> stk;
		
		//PreIter Constructor
      public PreIter()
      {
         stk = new MyStack<BSTNode>();
         if(root != null)
         {
            stk.push(root);
         }
      }
		
		//hasNext check to see if there is another element left in the BST to traverse
		//returns true if there is another element, false if no more element
      public boolean hasNext()
      {
         return !stk.isEmpty();
      }
		
		//next traverses the BST element by element pre-order
		//returns the next element object
      public T next()
      {
         BSTNode y;

         if(stk.isEmpty())
         {
            throw new NoSuchElementException();
         }

         y = stk.pop();

         if(y.right != null)
         {
            stk.push(y.right);
         }
         if(y.left != null)
         {
            stk.push(y.left);
         }

         return y.element;
      }
		
		//Unsupported remove method that throws exception if used
      public void remove()
      {
         throw new UnsupportedOperationException();
      }

   }
	
	//InIter Class
	//Iterator for inorder traversal of the BST
	private class InIter implements Iterator<T> 
   { 
      public MyStack<BSTNode> stk;
		
		//Support method for inorder traversal that for a given node x
		//it pushes into the stack all left descendants of x
      private void stackUpLefts(BSTNode x)
      {
         while(x.left != null)
         {
            stk.push(x.left);
            x = x.left;
         }
      }
		
		//InInter constructor
      public InIter()
      {
         stk = new MyStack<BSTNode>();
         if(root != null)
         {
            stk.push(root);
            stackUpLefts(root);
         }
      }
		
		//hasNext() checks to see if there is another element left in the BST to traverse
		//returns true if there is another element, false if empty
      public boolean hasNext()
      {
         return !stk.isEmpty();
      }
		
		//next() moves to the next element in the BST and returns the object.
      public T next()
      {
         BSTNode y;

         if(stk.isEmpty())
         {
            throw new NoSuchElementException();
         }
         y = stk.pop();
         if(y.right != null)
         {
            stk.push(y.right);
            stackUpLefts(y.right);
         }
         return y.element;
      }
		
		//Unsupported remove(), throws exception if used
      public void remove()
      {
         throw new UnsupportedOperationException();
      }
   }
		
	//LevelIter class	
	//Iterator for levelorder traversal of the BST
   private class LevelIter implements Iterator<T>
   {
      public LQueue<BSTNode> queue;
		
		//LevelIter object constructor
      public LevelIter()
      {
         queue = new LQueue<BSTNode>();

         if(root != null)
         {
            queue.enqueue(root);
  			}
      }
		
		//hasNext() checks to see if there is another element to be traversed
		//returns true if there is, and false if it is empty
      public boolean hasNext()
      {
         return !queue.isEmpty();
      }

		//next() moves to the next element in the BST, returns the Object at that position
		//The BST is traversed level order
      public T next()
      {
         BSTNode y;

         if(queue.isEmpty())
         {
            throw new NoSuchElementException();
         }
         y = queue.dequeue();

         if(y.left != null)
         {
            queue.enqueue(y.left);
         }
         if(y.right != null)
         {
            queue.enqueue(y.right);
         }

         return y.element;
      }
		
		//Unsupported remove(), throws exception if used
      public void remove()
      {
         throw new UnsupportedOperationException();
      }
   }
	
	//MyException Class
	//Private exception class that extends the runtimeexception class
   public static class MyException extends RuntimeException {
		
		//Constructors
      public MyException() {
         super();
      }
      public MyException(String msg) {
         super(msg);
      }

   }
	
	//BST constructor
   public BST() {

      root = null;		

   }

   //Insert
	//Inserts a node into the BST containing item passed as a parameter
   public void insert(T item) {

      root = insert(item, root);

   }

   //Recursive insert
	//The private version of insert to perform insert recursively
	//returns a BSTNode that contains the new tree after insertion
   private BSTNode insert(T item, BSTNode treeroot) {

      if(treeroot == null) {
         treeroot = new BSTNode();
         treeroot.element = item;
      }	
      else {
         if(item.compareTo(treeroot.element) < 0)
            treeroot.left = insert(item, treeroot.left);
         else
            treeroot.right = insert(item, treeroot.right);
      }

      return treeroot;

   }
	
	//Delete
	//Searches the BST for the target passed in as a parameter
	//Returns true if item is found, false if not found
   public void delete(T item) {

      root = delete(item, root);

   }
	
	//Recursive delete
	//Private version of delete to delete an element recursively
	//returns a BSTNode that contains the new tree after deletion
   private BSTNode delete(T item, BSTNode treeroot) {

      if(treeroot != null) {

         if(item.compareTo(treeroot.element) < 0) 
            treeroot.left = delete(item, treeroot.left);
         else if(item.compareTo(treeroot.element) > 0)
            treeroot.right = delete(item, treeroot.right);
         else
            treeroot = deleteNode(treeroot);
      }

      return treeroot;
   }
	
	//deleteNode
	//Helper method to the delete. This methods restructures the tree 
	//after deletion. Depending on how many children the deleted node has,
	//this method will take different actions.
	//returns the BSTNode containing new subtree after the deletion.
   private BSTNode deleteNode(BSTNode x)
   {
      T nextValue;
      BSTNode answer;

      if(x.left != null && x.right != null)
      {
         nextValue = successor(x);
         x.element = nextValue;
         x.right = delete(nextValue, x.right);
         answer = x;
      }
      else
      {
         if(x.left != null)
         {
            answer = x.left;
         }
         else
         {
            if(x.right != null)
            {
               answer = x.right;
            }
            else
            {
               answer = null;
            }
         }

      }

      return answer;
   }
			   		
   //Successor
	//Helper method to deleteNode method. This method finds the successor of
	//the deleted node. The successor node is the node containing the smallest value 
	//in the right subtree of x. Returns the successor data value.
   private T successor(BSTNode x)
   {
      BSTNode temp = x.right;

      while(temp.left != null)
      {
         temp = temp.left;
      }

      return temp.element;
   }           

   //Find
	//Finds a target passed in as a parameter and returns true if target is found,
	//or false if it is not found.
   public boolean find(T target) {

      return find(target, root);

   }

   //Recursive find
	//Private version of find, so find could be done recursively. Pass in the target
	//to be found and the root of the BST.
	//Returns true if target is found, false if not.
   private boolean find(T item, BSTNode treeroot) {
			
      boolean answer;
      if(treeroot == null) 
         answer = false;
      else {
         if(item.compareTo(treeroot.element) == 0)
            answer = true;
         else {
            if(item.compareTo(treeroot.element) < 0) 
               answer = find(item, treeroot.left);
            else
               answer = find(item, treeroot.right);
         }
      }

      return answer;

   }
	
	//isEmpty
	//Check to see if BST is empty.
	//Returns true if it is, false if not.
   public boolean isEmpty() {

      return root == null;

   }
	
   //makeEmpty
	//Clears the BST and make it empty.
	public void makeEmpty() {

      root = null;

   }
   
	//Size
	//Returns the number of elements in the BST.
   public int size() {

      return size(root);
   }
   
	//Recursive size
	//Private version of size so the number of elements could be found recursively.
   private int size(BSTNode treeroot) {
		
		int answer;

      if(treeroot == null)
         answer = 0;
      else
         answer = size(treeroot.left) + size(treeroot.right) + 1;
		
		return answer;
	
   }

   //Find minimum
	//Returns the minimum data value in the tree. If tree is empty, throws MyException.
   public T findMinimum() {

      if(root == null)
         throw new MyException();
      else {
         return findMinimum(root);
      }

   }

   //Find minimum recursive
   //Private version of findMinimum. Minimum value of the BST is found recursively.
	//Returns the minimum data value.
	private T findMinimum(BSTNode treeroot) {

      T answer;

      if(treeroot.left == null) {
         answer = treeroot.element;
      }
      else {
         answer = findMinimum(treeroot.left);
      }
      return answer;

   }

   //Find maximum
	//Returns the maximum data value in the tree. If tree is empty, throws MyException.
	public T findMaximum() {

      if(root == null)
         throw new MyException();
      else {
         return findMaximum(root);
      }

   }

   //Find maximum recursive
	//Private version of findMaximum. Maximum value of the BST is found recursively.
	//Returns the maximum data value.
   private T findMaximum(BSTNode treeroot) {

      T answer;

      if(treeroot.right == null) {
         answer = treeroot.element;
      }
      else {
         answer = findMaximum(treeroot.right);
      }
      return answer;

   }
	
	//IteratorPre
	//Public method to return an instance of the iteratorPre object
   public Iterator<T> iteratorPre() 
   {
      return new PreIter();
   }
	
	//IteratorIn
	//Public method to return an instance of the iteratorIn object
   public Iterator<T> iteratorIn() 
   {
      return new InIter();
   }

	//IteratorLevel
	//Public method to return an instance of the iteratorLevel object
   public Iterator<T> iteratorLevel()
   {
      return new LevelIter();
   }
	
	//printTree
	//Prints the BST with indentation representing tree level.
   public void printTree() {

      printTree(root, 0);
		System.out.println();

   }
	
	//printTree recursive
	//Private version of printTree. Printing is done recursively.
   private void printTree(BSTNode treeroot, int num) {

      if(treeroot != null) {
			
			//Print the appropriate amount of indentaion
			for(int i=0 ; i<num ; i++) {
				System.out.print("    ");
			}
			//Print element
         System.out.println(treeroot.element);
         printTree(treeroot.left, num+1);
         printTree(treeroot.right, num+1);
			
      }
		
   }
	
	//toString
	//Returns a string containing all the tree elements separated by spaces
   public String toString() {

      return toString(root);	

   }
	
	//toString recursive
	//Private version of toString. The string of elements is constructed recursively.
	//returns a String containing all the tree elements in 1 line.
   private String toString(BSTNode treeroot) {

      if(treeroot != null)
         return treeroot.element.toString().concat(" " + toString(treeroot.left) + toString(treeroot.right));
      else
         return "";
   }

}     
