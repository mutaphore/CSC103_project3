//BSTDriver.java
//Andy Chen, ID: 009433043 
//Michael Woodson, ID: 006470434
//Project 3
//CSC103 - Gharibyan - Section 11

import java.util.*;

//BSTDriver Class
//This is the client class used to test the functionality of the BST class
public class BSTDriver {
   
   public static void main(String[] args) {
    
   	Scanner input = new Scanner(System.in);
   	BST<Integer> bst = new BST<Integer>();
   	char choice = 'z';
   	Integer temp;
   
   	System.out.println("Choose one of the following operations by entering provided letter: ");
   	System.out.println("a - add the element \n" + 
     	    		   		 "d - delete the element \n" +
       			   		 "f - find the element \n" +
   			      		 "e - check if the tree is empty \n" +
   			      		 "k - make the tree empty \n" +
   		         		 "n - get the number of nodes (the size) of the tree \n" +
   							 "m - find the minimal element \n" +
   							 "x - find the maximal element \n" +
   							 "p - print the tree in preorder using iterator \n" +
   							 "i - print the tree in inorder using iterator \n" +
   							 "l - print the tree in levelorder using iterator \n" +
   							 "t - print the tree using printTree \n" +
   							 "o - output the tree using toString \n" +
   							 "q - Quit the program \n");
   
   	while(choice != 'q') {
   
   		System.out.print("Enter a choice: ");
   		choice = input.nextLine().charAt(0);
   
   		switch(choice) {
   			
   			case 'a':
   						System.out.println("Enter number to be added: ");
   						temp = input.nextInt();
   						input.nextLine();
   						bst.insert(temp);
   						System.out.println(temp + " has been added");
   						break;
   			case 'd': 
   						System.out.println("Enter number to be deleted: ");
   						temp = input.nextInt();
   						input.nextLine();
   						bst.delete(temp);
   						System.out.println(temp + " has been deleted");
   						break;
   			case 'f':
   						System.out.println("Enter number to find: ");
   						temp = input.nextInt();
   						input.nextLine();
   						if(bst.find(temp))
   							System.out.println(temp + " is found");
   						else
   							System.out.println(temp + " is NOT found");
   						break;
   			case 'e':
   						if(bst.isEmpty())
   							System.out.println("BST is empty");
   						else
   							System.out.println("BST is NOT empty");
   						break;
   			case 'k':
   						bst.makeEmpty();
   						System.out.println("Tree is now empty");
   						break;
   			case 'n':
   						System.out.println("Size of tree is " + bst.size());
   						break;
   			case 'm':
							try {
   							System.out.println("The minimal element is " + bst.findMinimum());
   						}
							catch(BST.MyException e) {
								System.out.println("BST is empty");
							}
							break;
   			case 'x':
							try {
   							System.out.println("The maximal element is " + bst.findMaximum());
   						}
							catch(BST.MyException e) {
								System.out.println("BST is empty");
							}
							break;
   			case 'p':
   						System.out.println("Pre-order traversal with Iterator: ");
   						Iterator<Integer> preItr = bst.iteratorPre();
   						while(preItr.hasNext()) 
   							System.out.print(preItr.next() + " ");
   						System.out.println();
   						break;
   			case 'i':
   						System.out.println("In-order traversal with Iterator: ");
   						Iterator<Integer> inItr = bst.iteratorIn();
   						while(inItr.hasNext())
   							System.out.print(inItr.next() + " ");
   						System.out.println();
   						break;
   			case 'l':
   						System.out.println("Level-order traversal with Iterator: ");
   						Iterator<Integer> levelItr = bst.iteratorLevel();
   						while(levelItr.hasNext())
   							System.out.print(levelItr.next() + " ");
   						System.out.println();
   						break;
   			case 't':
   						System.out.println("Print tree using printTree: ");
   						bst.printTree();
   						break;
   			case 'o':
   						System.out.println("Output tree with toString: ");
   						System.out.println(bst.toString());
   						break;
   			case 'q':
   						System.out.println("Farewell!");
   						break;
   			default:
   						System.out.println("Invalid choice!");
   						break;
   		}
   	}
   }   
}
