//MyStack.java
//Andy Chen, ID: 009433043 
//Michael Woodson, ID: 006470434
//Project 3
//CSC103 - Gharibyan - Section 11

//MyStack Generic class
//Uses a linked list to implement a stack ADT with push, pop, peek and isEmpty methods.
import java.util.*;

public class MyStack<T>{

   private class Node{
      public T element;
      public Node next;
   }
   
   private Node top; 
   
   public MyStack(){
      top = null;
   }
   
   //Pushes an element into a stack
   public void push(T data){
      Node newNode = new Node();
      newNode.element = data;
      newNode.next = top;
      top = newNode;
   }
   
   //Removes an element from stack and return the data in that element
   public T pop(){
      T temp;
      if(top != null){
         temp = top.element;
         top = top.next;
         return temp;
      }
      else{
         throw new EmptyStackException();
      }
   }
  
   //Returns the data in the top  
   public T peek(){
      T temp;
      if(top != null){
         temp = top.element;
         return temp;
      }
      else
         throw new EmptyStackException();         
   }
   //Checks to see if stack is empty. Returns TRUE if it is. 
   public boolean isEmpty(){
      return top == null;
   }
  
}
